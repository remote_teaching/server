package ru.hnau.remote_teaching_server

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import ru.hnau.remote_teaching_server.managers.internal.user.UserManager
import ru.hnau.remote_teaching_server.utils.FirebaseManager
import ru.hnau.remote_teaching_server.utils.log.HSessionLog

@SpringBootApplication
open class Application : CommandLineRunner {

    @Autowired
    private lateinit var userManager: UserManager

    override fun run(vararg args: String?) {
        FirebaseManager.init()
        userManager.createAdminIfNeed(HSessionLog("MAIN"))
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(Application::class.java, *args)
        }
    }

}

package ru.hnau.remote_teaching_server.api

import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_server.db.entities.UserDB
import ru.hnau.remote_teaching_server.utils.log.HSessionLog


class RequestContext(
        private val userInner: UserDB?,
        val logger: HSessionLog
) {

    val user: User by lazy { userInner!!.user }

}
package ru.hnau.remote_teaching_server.api

import ru.hnau.remote_teaching_server.managers.internal.user.UserManager


class RestServiceContext(
        val userManager: UserManager
)
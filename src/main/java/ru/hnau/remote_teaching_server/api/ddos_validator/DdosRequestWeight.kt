package ru.hnau.remote_teaching_server.api.ddos_validator


data class DdosRequestWeight(
        val value: Int
) {

    companion object {

        val LIGHT = DdosRequestWeight(1)
        val MEDIUM = DdosRequestWeight(2)
        val HARD = DdosRequestWeight(10)

    }

}
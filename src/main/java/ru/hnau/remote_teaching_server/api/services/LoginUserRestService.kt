package ru.hnau.remote_teaching_server.api.services

import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService
import ru.hnau.remote_teaching_server.managers.internal.ClientAppInstanceManager

@RestController
class LoginUserRestService : RestService() {

    @PatchMapping("/users/{login}/login")
    fun login(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("login") login: String,
            @RequestParam("password") password: String,
            @RequestParam("app-instance-uuid") appInstanceUUID: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, null) {

        Validators.validateUserLoginOrThrow(login)
        Validators.validateUserPasswordOrThrow(password)
        ClientAppInstanceManager.validateUUIDOrThrow(appInstanceUUID, logger)

        userManager.login(login, password, appInstanceUUID)
    }

    @GetMapping("/users/me")
    fun me(
            @RequestHeader headers: HttpHeaders
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, emptyList()) {

        user
    }

    @PatchMapping("/users/logout")
    fun logout(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("from-all-clients") fromAllClients: Boolean,
            @RequestParam("app-instance-uuid") appInstanceUUID: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, emptyList()) {

        ClientAppInstanceManager.validateUUIDOrThrow(appInstanceUUID, logger)

        userManager.logout(user.login, fromAllClients, appInstanceUUID)
    }

}
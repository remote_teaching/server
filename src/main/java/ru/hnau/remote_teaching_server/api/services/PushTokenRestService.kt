package ru.hnau.remote_teaching_server.api.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService
import ru.hnau.remote_teaching_server.managers.internal.ClientAppInstanceManager


@RestController
class PushTokenRestService : RestService() {

    @Autowired
    private lateinit var clientAppInstanceManager: ClientAppInstanceManager

    @PatchMapping("/client-app-instance/{app-instance-uuid}/push-token/{push-token}")
    fun changePassword(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("app-instance-uuid") appInstanceUUID: String,
            @PathVariable("push-token") pushToken: String
    ) = handleRequest(
            headers, DdosRequestWeight.MEDIUM, null
    ) {

        ClientAppInstanceManager.validateUUIDOrThrow(appInstanceUUID, logger)
        ClientAppInstanceManager.validatePushTokenOrThrow(pushToken, logger)

        clientAppInstanceManager.updatePushToken(appInstanceUUID, pushToken)
    }

}
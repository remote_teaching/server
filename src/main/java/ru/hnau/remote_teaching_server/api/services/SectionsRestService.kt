package ru.hnau.remote_teaching_server.api.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_common.data.UserPermission
import ru.hnau.remote_teaching_common.data.section.SectionContentMDUpdateParam
import ru.hnau.remote_teaching_common.data.section.SectionInfo
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService
import ru.hnau.remote_teaching_server.managers.internal.SectionManager
import ru.hnau.remote_teaching_server.managers.internal.TestManager


@RestController
class SectionsRestService : RestService() {

    @Autowired
    private lateinit var sectionManager: SectionManager

    @Autowired
    private lateinit var testManager: TestManager

    @GetMapping("/sections/{section-uuid}/info")
    fun getSectionInfo(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("section-uuid") sectionUUID: String
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, emptyList()) {
        val uuid = SectionManager.validateAndPrepareSectionUUIDFromClient(sectionUUID)
        SectionInfo(
                contentMD = sectionManager.getContentMD(uuid),
                subsections = sectionManager.getSubsections(uuid),
                tests = testManager.getTestsOfSection(uuid)
        )
    }

    @PutMapping("/sections/{section-uuid}")
    fun addSubsection(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("section-uuid") sectionUUID: String,
            @RequestParam("title") title: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        val uuid = SectionManager.validateAndPrepareSectionUUIDFromClient(sectionUUID)
        Validators.validateSectionTitleOrThrow(title)
        sectionManager.addSubsection(uuid, title)
    }

    @DeleteMapping("/sections/{section-uuid}")
    fun removeSection(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("section-uuid") sectionUUID: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        val uuid = SectionManager.validateAndPrepareSectionUUIDFromClient(sectionUUID)
        sectionManager.removeSection(uuid)
    }

    @PatchMapping("/sections/{section-uuid}/title/{title}")
    fun updateTitle(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("section-uuid") sectionUUID: String,
            @PathVariable("title") title: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        val uuid = SectionManager.validateAndPrepareSectionUUIDFromClient(sectionUUID)
        Validators.validateSectionTitleOrThrow(title)
        sectionManager.updateTitle(uuid, title)
    }

    @PatchMapping("/sections/{section-uuid}/content-md")
    fun updateContentMD(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("section-uuid") sectionUUID: String,
            @RequestBody contentMDUpdateParam: SectionContentMDUpdateParam
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        val uuid = SectionManager.validateAndPrepareSectionUUIDFromClient(sectionUUID)
        sectionManager.updateContentMD(uuid, contentMDUpdateParam.contentMD)
    }

}
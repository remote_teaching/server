package ru.hnau.remote_teaching_server.api.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_common.data.UserPermission
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService
import ru.hnau.remote_teaching_server.managers.internal.user.StudentManager
import ru.hnau.remote_teaching_server.managers.internal.user.TeacherManager


@RestController
class TeachersListRestService : RestService() {

    @Autowired
    private lateinit var teacherManager: TeacherManager

    @GetMapping("/teachers")
    fun getAll(
            @RequestHeader headers: HttpHeaders
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, listOf(UserPermission.MANAGE_TEACHERS)) {

        teacherManager.getAllTeachers()

    }

    @DeleteMapping("/teachers/{teacher-login}")
    fun remove(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("teacher-login") teacherLogin: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_TEACHERS)) {

        Validators.validateUserLoginOrThrow(teacherLogin)

        teacherManager.removeTeacher(teacherLogin)
    }


    @PatchMapping("/teachers/{teacher-login}/restore-password-code")
    fun restorePasswordCode(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("teacher-login") teacherLogin: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_TEACHERS)) {

        Validators.validateUserLoginOrThrow(teacherLogin)

        teacherManager.generateRestoreTeacherPasswordActionCode(
                login = teacherLogin,
                sessionLogger = logger
        )
    }

    @PatchMapping("/teachers/me/restore-password")
    fun restorePassword(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("new-password") newPassword: String,
            @RequestParam("action-code") actionCode: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, null) {

        Validators.validateUserPasswordOrThrow(newPassword)

        teacherManager.restoreTeacherPassword(
                code = actionCode,
                newPassword = newPassword,
                sessionLogger = logger
        )
    }

    @PutMapping("/teachers")
    fun create(
            @RequestHeader headers: HttpHeaders
    ) = handleRequest(headers, DdosRequestWeight.MEDIUM, listOf(UserPermission.MANAGE_TEACHERS, UserPermission.MANAGE_GROUPS)) {

        teacherManager.generateCreateTeacherActionCode(
                sessionLogger = logger
        )
    }

    @PutMapping("/teachers/{login}")
    fun register(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("login") login: String,
            @RequestParam("password") password: String,
            @RequestParam("action-code") actionCode: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, null) {

        Validators.validateUserLoginOrThrow(login)
        Validators.validateUserPasswordOrThrow(password)

        teacherManager.createTeacher(
                login = login,
                actionCode = actionCode,
                password = password,
                sessionLogger = logger
        )
    }

}
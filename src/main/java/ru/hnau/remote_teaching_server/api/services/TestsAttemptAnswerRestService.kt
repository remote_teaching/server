package ru.hnau.remote_teaching_server.api.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_common.data.UserPermission
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService
import ru.hnau.remote_teaching_server.managers.internal.TestAttemptAnswerManager
import ru.hnau.remote_teaching_server.managers.internal.TestAttemptManager


@RestController
class TestsAttemptAnswerRestService : RestService() {

    @Autowired
    private lateinit var testAttemptAnswerManager: TestAttemptAnswerManager

    @PutMapping("/tests-attempts/{test-attempt-uuid}/answer")
    fun setTestAttemptAnswer(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-attempt-uuid") testAttemptUUID: String,
            @RequestBody answer: List<List<String>>
    ) = handleRequest(headers, DdosRequestWeight.HARD, emptyList()) {
        testAttemptAnswerManager.addAnswer(
                student = user,
                testAttemptUUID = testAttemptUUID,
                answer = answer
        )
    }

    @GetMapping("/tests/{test-uuid}/students-group/{students-group-name}/results")
    fun getTestStudentsGroupResults(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-uuid") testUUID: String,
            @PathVariable("students-group-name") studentsGroupName: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_STUDENTS)) {
        testAttemptAnswerManager.getStudentsGroupTestResults(
                testUUID = testUUID,
                studentsGroupName = studentsGroupName
        )
    }


}
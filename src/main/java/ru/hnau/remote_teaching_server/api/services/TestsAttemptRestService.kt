package ru.hnau.remote_teaching_server.api.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_common.data.UserPermission
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService
import ru.hnau.remote_teaching_server.managers.internal.TestAttemptManager


@RestController
class TestsAttemptRestService : RestService() {

    @Autowired
    private lateinit var testAttemptManager: TestAttemptManager

    @PutMapping("/tests/{test-uuid}/attempts")
    fun addTestAttempt(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-uuid") testUUID: String,
            @RequestParam("students-group-name") studentsGroupName: String,
            @RequestParam("timeLimit") timeLimit: Long
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_GROUPS)) {
        Validators.validateTestTimeLimitOrThrow(timeLimit)
        testAttemptManager.addAttempt(testUUID, studentsGroupName, timeLimit, logger)
    }

    @GetMapping("/tests-attempts/my")
    fun getAvailableTestAttempts(
            @RequestHeader headers: HttpHeaders
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, emptyList()) {
        testAttemptManager.getAvailableTestAttemptsForStudent(user)
    }

    @GetMapping("/tests-attempts/{test-attempt-uuid}/tasks-compilation")
    fun getTestAttemptTasksCompilation(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-attempt-uuid") testAttemptUUID: String
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, emptyList()) {
        testAttemptManager.getTestAttemptTasksCompilation(
                testAttemptUUID = testAttemptUUID,
                student = user
        )
    }


}
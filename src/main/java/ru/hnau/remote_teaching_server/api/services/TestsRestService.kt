package ru.hnau.remote_teaching_server.api.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_common.data.UserPermission
import ru.hnau.remote_teaching_common.data.test.TestTask
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService
import ru.hnau.remote_teaching_server.managers.internal.TestManager


@RestController
class TestsRestService : RestService() {

    @Autowired
    private lateinit var testManager: TestManager

    @GetMapping("/tests/{test-uuid}/tasks")
    fun getTestTasks(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-uuid") testUUID: String
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, emptyList()) {
        testManager.getTasks(testUUID)
    }

    @PutMapping("/tests/of-section/{section-uuid}")
    fun addTest(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("section-uuid") sectionUUID: String,
            @RequestParam("title") title: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        Validators.validateTestTitleOrThrow(title)
        testManager.addTest(sectionUUID, title)
    }

    @DeleteMapping("/tests/{test-uuid}")
    fun removeTest(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-uuid") testUUID: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        testManager.removeTest(testUUID)
    }

    @PatchMapping("/tests/{test-uuid}/title/{title}")
    fun updateTitle(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-uuid") testUUID: String,
            @PathVariable("title") title: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        Validators.validateTestTitleOrThrow(title)
        testManager.updateTitle(testUUID, title)
    }

    @PatchMapping("/tests/{test-uuid}/time-limit/{time-limit}")
    fun updateTimeLimit(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-uuid") testUUID: String,
            @PathVariable("time-limit") timeLimit: Long
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        Validators.validateTestTimeLimitOrThrow(timeLimit)
        testManager.updateTimeLimit(testUUID, timeLimit)
    }

    @PatchMapping("/tests/{test-uuid}/pass-percentage/{pass-percentage}")
    fun updatePassPercentage(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-uuid") testUUID: String,
            @PathVariable("pass-percentage") passPercentage: Float
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        Validators.validateTestPassPercentageOrThrow(passPercentage)
        testManager.updatePassScorePercentage(testUUID, passPercentage)
    }

    @PatchMapping("/tests/{test-uuid}/tasks")
    fun updateTasks(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("test-uuid") testUUID: String,
            @RequestBody tasks: List<TestTask>
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_SECTIONS)) {
        Validators.validateTestTasksOrThrow(tasks)
        testManager.updateTasks(testUUID, tasks)
    }

}
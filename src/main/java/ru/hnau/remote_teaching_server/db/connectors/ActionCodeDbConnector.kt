package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.connectors.utils.query.lt
import ru.hnau.remote_teaching_server.db.entities.ActionCodeDB


@Component
class ActionCodeDbConnector : DbConnector<ActionCodeDB, String>(
        entityFabric = ActionCodeDB
) {

    fun put(actionCode: ActionCodeDB) {
        save(actionCode)
    }

    fun findOrNull(type: ActionCodeType, code: String) =
            findFirstOrNull(checkID(type, code))

    fun checkIsExists(type: ActionCodeType, code: String) =
            checkIsExists(checkID(type, code))

    fun remove(type: ActionCodeType, code: String) =
            remove(checkID(type, code)) > 0

    fun removeAllWithTypeAndCreatedBeforeTime(
            type: ActionCodeType,
            beforeTime: Long
    ) = remove(
            checkType(type) +
                    (ActionCodeDB.CREATED_KEY lt beforeTime)
    )

    fun removeAllWithTypeAndAdditionalData(
            type: ActionCodeType,
            additionalData: String
    ) {
        remove(
                checkType(type) +
                        (ActionCodeDB.ADDITIONAL_DATA_KEY eq additionalData)
        )
    }

    private fun checkID(type: ActionCodeType, code: String) =
            checkID(ActionCodeDB.createKey(type, code))

    private fun checkType(type: ActionCodeType) =
            ActionCodeDB.TYPE_KEY eq type

}
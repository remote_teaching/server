package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.connectors.utils.query.ne
import ru.hnau.remote_teaching_server.db.connectors.utils.update.set
import ru.hnau.remote_teaching_server.db.entities.ClientAppInstanceDB


@Component
class ClientAppInstanceDbConnector : DbConnector<ClientAppInstanceDB, String>(
        entityFabric = ClientAppInstanceDB
) {

    fun cleanOutLogin(
            login: String
    ) = updateAll(
            ClientAppInstanceDB.USER_LOGIN_KEY eq login,
            ClientAppInstanceDB.USER_LOGIN_KEY set ""
    )

    fun updateLogin(
            clientAppInstanceUUID: String,
            login: String
    ) = createOrUpdate(
            clientAppInstanceUUID = clientAppInstanceUUID,
            fieldKey = ClientAppInstanceDB.USER_LOGIN_KEY,
            fieldValue = login,
            loginForNewInstance = login,
            pushTokenForNewInstance = "",
            isNeedUpdateChecker = { it.userLogin != login }
    )

    fun updatePushToken(
            clientAppInstanceUUID: String,
            pushToken: String
    ) = createOrUpdate(
            clientAppInstanceUUID = clientAppInstanceUUID,
            fieldKey = ClientAppInstanceDB.PUSH_TOKEN_KEY,
            fieldValue = pushToken,
            loginForNewInstance = "",
            pushTokenForNewInstance = pushToken,
            isNeedUpdateChecker = { it.pushToken != pushToken }
    )

    fun getPushTokensForLogin(login: String) =
            findMany(
                    (ClientAppInstanceDB.USER_LOGIN_KEY eq login) +
                            (ClientAppInstanceDB.PUSH_TOKEN_KEY ne "")
            ).mapNotNull { it.pushToken }

    private fun createOrUpdate(
            clientAppInstanceUUID: String,
            fieldKey: String,
            fieldValue: Any,
            loginForNewInstance: String,
            pushTokenForNewInstance: String,
            isNeedUpdateChecker: (ClientAppInstanceDB) -> Boolean
    ) {
        val existence =
                findFirstOrNull(checkID(clientAppInstanceUUID))

        if (existence != null) {
            if (!isNeedUpdateChecker.invoke(existence)) {
                return
            }
            updateFirst(checkID(clientAppInstanceUUID), fieldKey set fieldValue)
            return
        }

        val new = ClientAppInstanceDB(
                uuid = clientAppInstanceUUID,
                pushToken = pushTokenForNewInstance,
                userLogin = loginForNewInstance
        )
        save(new)
    }

}
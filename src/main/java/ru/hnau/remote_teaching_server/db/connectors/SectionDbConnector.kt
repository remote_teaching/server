package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.DbRawQueryBuilder
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.connectors.utils.update.set
import ru.hnau.remote_teaching_server.db.entities.SectionDB


@Component
class SectionDbConnector : DbConnector<SectionDB, String>(
        entityFabric = SectionDB
) {

    fun getSubskeletons(sectionUUID: String) = findMany(
            (SectionDB.PARENT_UUID_KEY eq sectionUUID).toRaw().removeField(SectionDB.CONTENT_MD_KEY)
    ).map(SectionDB::getSkeleton)


    fun getContentMD(sectionUUID: String) = findFirstOrThrow(
            checkID(sectionUUID).toRaw()
                    .removeField(SectionDB.UUID_KEY)
                    .removeField(SectionDB.PARENT_UUID_KEY)
                    .removeField(SectionDB.TITLE_KEY)
    ).contentMD!!

    fun addSection(section: SectionDB) =
            save(section)

    fun removeSection(sectionUUID: String) {
        remove(checkID(sectionUUID))
    }

    fun updateTitle(
            sectionUUID: String,
            title: String
    ) {
        updateFirst(
                checkID(sectionUUID),
                SectionDB.TITLE_KEY set title
        )
    }

    fun updateContentMD(
            sectionUUID: String,
            contentMD: String
    ) {
        updateFirst(
                checkID(sectionUUID),
                SectionDB.CONTENT_MD_KEY set contentMD
        )
    }

}
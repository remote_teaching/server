package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.connectors.utils.update.set
import ru.hnau.remote_teaching_server.db.entities.StudentsGroupDB


@Component
class StudentsGroupDbConnector : DbConnector<StudentsGroupDB, String>(
        entityFabric = StudentsGroupDB
) {

    fun getAll() =
            findAll()

    fun create(name: String) =
            save(StudentsGroupDB(name, false))

    fun throwIfAlreadyExists(name: String) =
            throwIfExists(checkID(name))

    fun throwIfNotExists(name: String) =
            throwIfNotExists(checkID(name))

    fun throwIfNotArchived(name: String) = throwIfNotExists(
            checkID(name) +
                    (StudentsGroupDB.ARCHIVED_KEY eq true)
    )

    fun throwIfArchived(name: String) = throwIfNotExists(
            checkID(name) +
                    (StudentsGroupDB.ARCHIVED_KEY eq false)
    )

    fun getOrThrow(name: String) =
            findFirstOrThrow(checkID(name))

    fun getNotArchivedOrThrow(name: String) = findFirstOrThrow(
            checkID(name) +
                    (StudentsGroupDB.ARCHIVED_KEY eq false)
    )

    fun setIsArchived(
            name: String,
            archived: Boolean
    ) = updateFirst(
            checkID(name),
            StudentsGroupDB.ARCHIVED_KEY set archived
    )

    fun remove(name: String) =
            remove(checkID(name)) > 0


}
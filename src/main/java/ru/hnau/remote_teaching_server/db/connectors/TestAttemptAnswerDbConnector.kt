package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.entities.TestAttemptAnswerDB


@Component
class TestAttemptAnswerDbConnector : DbConnector<TestAttemptAnswerDB, String>(
        entityFabric = TestAttemptAnswerDB
) {

    private fun checkID(
            testAttemptUUID: String?,
            studentLogin: String?
    ) = checkID(
            TestAttemptAnswerDB.createKey(
                    testAttemptUUID = testAttemptUUID,
                    studentLogin = studentLogin
            )
    )

    fun addAnswer(
            answer: TestAttemptAnswerDB
    ) = save(
            entity = answer
    )

    fun getAttemptAnswers(
            testAttemptUUID: String
    ) = findMany(
            TestAttemptAnswerDB.TEST_ATTEMPT_UUID_KEY eq testAttemptUUID
    )

    fun throwIfAlreadyExists(
            testAttemptUUID: String,
            studentLogin: String
    ) = throwIfExists(
            checkID(
                    testAttemptUUID = testAttemptUUID,
                    studentLogin = studentLogin
            )
    )

    fun checkIsExists(
            testAttemptUUID: String,
            studentLogin: String
    ) = checkIsExists(
            checkID(
                    testAttemptUUID = testAttemptUUID,
                    studentLogin = studentLogin
            )
    )

    fun getOrNull(
            testAttemptUUID: String,
            studentLogin: String
    ) = findFirstOrNull(
            checkID(
                    testAttemptUUID = testAttemptUUID,
                    studentLogin = studentLogin
            )
    )

    fun removeAllStudentAnswers(
            studentLogin: String
    ) = remove(
            TestAttemptAnswerDB.STUDENT_LOGIN_KEY eq studentLogin
    )

    fun removeAllAttemptAnswers(
            testAttemptUUID: String
    ) = remove(
            TestAttemptAnswerDB.TEST_ATTEMPT_UUID_KEY eq testAttemptUUID
    )

}
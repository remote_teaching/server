package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.entities.TestAttemptDB


@Component
class TestAttemptDbConnector : DbConnector<TestAttemptDB, String>(
        entityFabric = TestAttemptDB
) {

    fun addAttempt(attempt: TestAttemptDB) =
            save(attempt)

    fun getAttemptsForStudentsGroup(
            studentsGroupName: String
    ) = findMany(
            TestAttemptDB.STUDENTS_GROUP_NAME_KEY eq studentsGroupName
    )

    fun getTestAttemptsForStudentGroup(
            testUUID: String,
            studentsGroupName: String
    ) = findMany(
            (TestAttemptDB.STUDENTS_GROUP_NAME_KEY eq studentsGroupName) +
                    (TestAttemptDB.TEST_UUID_KEY eq testUUID)
    )

    fun getTestAttempts(
            testUUID: String
    ) = findMany(
            TestAttemptDB.TEST_UUID_KEY eq testUUID
    )

    fun getAttempt(
            testAttemptUUID: String
    ) = findFirstOrThrow(
            checkID(testAttemptUUID)
    )

    fun getStudentsGroupAttempts(
            studentsGroupName: String
    ) = findMany(
            TestAttemptDB.STUDENTS_GROUP_NAME_KEY eq studentsGroupName
    )

    fun removeStudentsGroupAttempts(
            studentsGroupName: String
    ) = remove(
            TestAttemptDB.STUDENTS_GROUP_NAME_KEY eq studentsGroupName
    )

    fun removeTestAttempts(
            testUUID: String
    ) = remove(
            TestAttemptDB.TEST_UUID_KEY eq testUUID
    )

}
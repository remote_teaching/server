package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_common.data.test.TestTask
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.connectors.utils.update.set
import ru.hnau.remote_teaching_server.db.entities.TestDB


@Component
class TestDbConnector : DbConnector<TestDB, String>(
        entityFabric = TestDB
) {

    fun getTest(testUUID: String) =
            findFirstOrThrow(checkID(testUUID))

    fun getTestsOfSection(sectionUUID: String) = findMany(
            (TestDB.SECTION_UUID_KEY eq sectionUUID).toRaw().removeField(TestDB.TASKS_KEY)
    ).map(TestDB::getSkeleton)


    fun getTasks(uuid: String) = findFirstOrThrow(
            checkID(uuid).toRaw()
                    .removeField(TestDB.UUID_KEY)
                    .removeField(TestDB.SECTION_UUID_KEY)
                    .removeField(TestDB.TITLE_KEY)
                    .removeField(TestDB.PASS_SCORE_PERCENTAGE_KEY)
                    .removeField(TestDB.TIME_LIMIT_KEY)
    ).tasks!!

    fun getTitle(uuid: String) = findFirstOrThrow(
            checkID(uuid).toRaw()
                    .removeField(TestDB.UUID_KEY)
                    .removeField(TestDB.SECTION_UUID_KEY)
                    .removeField(TestDB.PASS_SCORE_PERCENTAGE_KEY)
                    .removeField(TestDB.TASKS_KEY)
                    .removeField(TestDB.TIME_LIMIT_KEY)
    ).title!!

    fun addTest(test: TestDB) =
            save(test)

    fun removeTest(uuid: String) {
        remove(checkID(uuid))
    }

    fun removeAllTestsOfSection(sectionUUID: String) =
            remove(TestDB.SECTION_UUID_KEY eq sectionUUID)

    fun updatePassScorePercentage(
            uuid: String,
            passScorePercentage: Float
    ) {
        updateFirst(
                checkID(uuid),
                TestDB.PASS_SCORE_PERCENTAGE_KEY set passScorePercentage
        )
    }

    fun updateTasks(
            uuid: String,
            tasks: List<TestTask>
    ) {
        updateFirst(
                checkID(uuid),
                TestDB.TASKS_KEY set tasks
        )
    }

    fun updateTitle(
            uuid: String,
            title: String
    ) {
        updateFirst(
                checkID(uuid),
                TestDB.TITLE_KEY set title
        )
    }

    fun updateTimeLimit(
            uuid: String,
            timeLimit: Long
    ) {
        updateFirst(
                checkID(uuid),
                TestDB.TIME_LIMIT_KEY set timeLimit
        )
    }

    fun throwIfNotExists(testUUID: String) =
            throwIfNotExists(checkID(testUUID))

}
package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_common.data.UserRole
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.connectors.utils.update.set
import ru.hnau.remote_teaching_server.db.entities.UserDB


@Component
class UserDbConnector : DbConnector<UserDB, String>(
        entityFabric = UserDB
) {

    fun put(user: UserDB) =
            save(user)

    fun remove(login: String) =
            remove(checkID(login)) > 0

    fun throwIfNotStudent(login: String) = throwIfNotExists(
            checkID(login) +
                    (UserDB.ROLE_KEY eq UserRole.STUDENT)
    )

    fun throwIfNotTeacher(login: String) = throwIfNotExists(
            checkID(login) +
                    (UserDB.ROLE_KEY eq UserRole.TEACHER)
    )

    fun removeAllStudentsOfGroup(studentsGroupName: String) =
            remove(UserDB.STUDENTS_GROUP_NAME_KEY eq studentsGroupName)

    fun updateAuthToken(
            login: String,
            authToken: String
    ) = updateFirst(
            checkID(login),
            UserDB.AUTH_TOKEN_KEY set authToken
    )

    fun checkAdminIsExists() =
            checkIsExists(UserDB.ROLE_KEY eq UserRole.ADMIN)

    fun throwIfAlreadyExists(login: String) =
            throwIfExists(checkID(login))

    fun getByLoginOrThrow(login: String) =
            findFirstOrThrow(checkID(login))

    fun getByLoginAndPasswordHashOrNull(
            login: String,
            passwordHash: String
    ) = findFirstOrNull(
            checkID(login) + (UserDB.PASSWORD_HASH_KEY eq passwordHash)
    )

    fun getByAuthTokenOrNull(authToken: String) =
            findFirstOrNull(UserDB.AUTH_TOKEN_KEY eq authToken)

    fun findAllStudentsOfGroup(
            studentsGroupName: String
    ) = findMany(
            (UserDB.ROLE_KEY eq UserRole.STUDENT) +
                    (UserDB.STUDENTS_GROUP_NAME_KEY eq studentsGroupName)
    )

    fun findAllTeachers() =
            findMany(UserDB.ROLE_KEY eq UserRole.TEACHER)

    fun updatePassword(
            login: String,
            newPasswordHash: String
    ) = updateFirst(
            checkID(login),
            UserDB.PASSWORD_HASH_KEY set newPasswordHash
    )

    fun updateFio(
            login: String,
            newName: String,
            newSurname: String,
            newPatronymic: String
    ) = updateFirst(
            checkID(login),
            (UserDB.NAME_KEY set newName) +
                    (UserDB.SURNAME_KEY set newSurname) +
                    (UserDB.PATRONYMIC_KEY set newPatronymic)
    )


}
package ru.hnau.remote_teaching_server.db.connectors.utils

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.MongoTemplate
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.ifTrue
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.db.connectors.utils.query.DbQueryBuilder
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.connectors.utils.update.DbUpdateBuilder
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric


abstract class DbConnector<T : Any, ID : Any>(
        entityFabric: EntityFabric<T>
) {

    private val entityIDKey = entityFabric.entityIDKey
    private val entityClass = entityFabric.entityClass
    private val entityName = entityFabric.entityName

    @Autowired
    protected lateinit var db: MongoTemplate

    /**
     * Utils
     */

    protected fun checkID(id: ID) =
            entityIDKey eq id

    /**
     * Save
     */

    protected fun save(entity: T): T =
            db.save(entity)

    /**
     * Exists
     */

    protected fun checkIsExists(queryBuilder: DbQueryBuilder) =
            db.exists(queryBuilder.buildQuery(), entityClass)

    protected fun throwIfExists(queryBuilder: DbQueryBuilder) {
        checkIsExists(queryBuilder).ifTrue { throwAlreadyExists() }
    }

    protected fun throwIfNotExists(queryBuilder: DbQueryBuilder) {
        checkIsExists(queryBuilder).ifFalse { throwNotExists() }
    }


    /**
     * Find first
     */

    protected fun findFirstOrNull(queryBuilder: DbQueryBuilder): T? =
            db.findOne(queryBuilder.buildQuery(), entityClass)

    protected fun findFirstOrThrow(queryBuilder: DbQueryBuilder): T =
            findFirstOrNull(queryBuilder) ?: throwNotExists()


    /**
     * Find many
     */

    protected fun findMany(queryBuilder: DbQueryBuilder): List<T> =
            db.find(queryBuilder.buildQuery(), entityClass)

    protected fun findAll(): List<T> =
            db.findAll(entityClass)


    /**
     * Delete
     */

    protected fun remove(queryBuilder: DbQueryBuilder): Long =
            db.remove(queryBuilder.buildQuery(), entityClass).deletedCount


    /**
     * Update
     */

    protected fun updateAll(
            queryBuilder: DbQueryBuilder,
            updateBuilder: DbUpdateBuilder
    ) = db.updateMulti(
            queryBuilder.buildQuery(),
            updateBuilder.buildUpdate(),
            entityClass
    ).modifiedCount

    protected fun updateFirst(
            queryBuilder: DbQueryBuilder,
            updateBuilder: DbUpdateBuilder
    ) = db.updateFirst(
            queryBuilder.buildQuery(),
            updateBuilder.buildUpdate(),
            entityClass
    ).matchedCount > 0

    /**
     * Private
     */

    private fun throwNotExists(): Nothing =
            throw ApiException.raw("$entityName отсутствует")

    private fun throwAlreadyExists(): Nothing =
            throw ApiException.raw("$entityName уже существует")


}
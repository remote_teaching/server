package ru.hnau.remote_teaching_server.db.connectors.utils.query

import org.springframework.data.mongodb.core.query.Query


class DbRawQueryBuilder(
        private val query: Query
) : DbQueryBuilder {

    companion object {

        val EMPTY = DbRawQueryBuilder(Query())

    }

    override fun buildQuery() =
            query

    fun removeField(field: String) =
            apply { query.fields().exclude(field) }

}
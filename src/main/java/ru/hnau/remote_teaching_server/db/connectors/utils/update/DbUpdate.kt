package ru.hnau.remote_teaching_server.db.connectors.utils.update


data class DbUpdate(
        val key: String,
        val value: Any
)


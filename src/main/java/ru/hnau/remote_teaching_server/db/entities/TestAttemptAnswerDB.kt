package ru.hnau.remote_teaching_server.db.entities

import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.remote_teaching_server.db.entities.utils.BindingCollection
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric


@Document(collection = "test_attempt_answer")
class TestAttemptAnswerDB(
        val testAttemptUUID: String? = null,
        val studentLogin: String? = null,
        val timestamp: Long? = null,
        val answer: List<List<String>>? = null
) : BindingCollection(
        key = createKey(testAttemptUUID, studentLogin)
) {

    companion object : EntityFabric<TestAttemptAnswerDB> {

        const val KEY_KEY = BindingCollection.KEY_KEY
        const val TEST_ATTEMPT_UUID_KEY = "testAttemptUUID"
        const val STUDENT_LOGIN_KEY = "studentLogin"
        const val TIMESTAMP_KEY = "timestamp"
        const val ANSWER_KEY = "answer"

        override val entityIDKey = KEY_KEY
        override val entityClass = TestAttemptAnswerDB::class.java
        override val entityName = "Ответ"

        fun createKey(
                testAttemptUUID: String?,
                studentLogin: String?
        ) = BindingCollection.createKey(
                testAttemptUUID,
                studentLogin
        )

    }

}
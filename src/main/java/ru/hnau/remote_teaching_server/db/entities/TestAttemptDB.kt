package ru.hnau.remote_teaching_server.db.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.takeIfPositive
import ru.hnau.remote_teaching_common.data.test.TestSkeleton
import ru.hnau.remote_teaching_common.data.test.TestTask
import ru.hnau.remote_teaching_common.data.test.attempt.TestAttempt
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric


@Document(collection = "test_attempt")
data class TestAttemptDB(
        @Id
        val uuid: String? = null,
        val testUUID: String? = null,
        val studentsGroupName: String? = null,
        val timestamp: Long? = null,
        val timeLimit: Long? = null
) {

    companion object : EntityFabric<TestAttemptDB> {

        const val UUID_KEY = "uuid"
        const val TEST_UUID_KEY = "testUUID"
        const val STUDENTS_GROUP_NAME_KEY = "studentsGroupName"
        const val TIMESTAMP_KEY = "timestamp"
        const val TIME_LIMIT_KEY = "timeLimit"

        override val entityIDKey = UUID_KEY
        override val entityClass = TestAttemptDB::class.java
        override val entityName = "Попытка"

    }

    val timeLeft: Long
        get() = (timestamp!! + timeLimit!!) - System.currentTimeMillis()

    val finished: Boolean
        get() = timeLeft <= 0

    fun throwIfFinished() {
        finished.ifTrue { throw ApiException.raw("Время попытки вышло") }
    }

    fun getAttempt(
            testTitle: String,
            timeLeft: Long
    ) = TestAttempt(
            uuid = uuid!!,
            testUUID = testUUID!!,
            studentsGroupName = studentsGroupName!!,
            timeLeft = timeLeft,
            testTitle = testTitle
    )

}
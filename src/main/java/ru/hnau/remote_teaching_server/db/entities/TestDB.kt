package ru.hnau.remote_teaching_server.db.entities

import org.apache.commons.codec.digest.DigestUtils
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.annotation.Id
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.test.TestSkeleton
import ru.hnau.remote_teaching_common.data.test.TestTask
import ru.hnau.remote_teaching_server.db.entities.UserDB.Companion.LOGIN_KEY
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric


@Document(collection = "test")
data class TestDB(
        @Id
        val uuid: String? = null,
        val sectionUUID: String? = null,
        val title: String? = null,
        val passScorePercentage: Float? = null,
        val tasks: List<TestTask>? = null,
        val timeLimit: Long? = null
) {

    companion object : EntityFabric<TestDB> {

        const val UUID_KEY = "uuid"
        const val SECTION_UUID_KEY = "sectionUUID"
        const val TITLE_KEY = "title"
        const val PASS_SCORE_PERCENTAGE_KEY = "passScorePercentage"
        const val TASKS_KEY = "tasks"
        const val TIME_LIMIT_KEY = "timeLimit"

        override val entityIDKey = UUID_KEY
        override val entityClass = TestDB::class.java
        override val entityName = "Тест"

    }

    val maxScore: Int
        get() = tasks!!.sumBy(TestTask::maxScore)

    fun getSkeleton() = TestSkeleton(
            uuid = uuid!!,
            sectionUUID = sectionUUID!!,
            passScorePercentage = passScorePercentage!!,
            title = title!!,
            timeLimit = timeLimit!!
    )

}
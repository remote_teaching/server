package ru.hnau.remote_teaching_server.managers.internal

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.takeIfPositive
import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_common.utils.ActionCodeUtils
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_server.db.connectors.ActionCodeDbConnector
import ru.hnau.remote_teaching_server.db.entities.ActionCodeDB
import ru.hnau.remote_teaching_server.utils.log.HSessionLog


@Component
class ActionCodeManager {

    companion object {

        private const val GENERATE_CODE_ATTEMPTS_COUNT = 1000
        private const val INSERTIONS_COUNT_UP_TO_CLEAN = 100

        private val insertionsCount = HashMap<ActionCodeType, Int>()

    }

    @Autowired
    private lateinit var dbConnector: ActionCodeDbConnector

    fun create(
            type: ActionCodeType,
            additionalData: String = "",
            sessionLogger: HSessionLog
    ): String {
        cleanUpIfNeed(type, sessionLogger)

        if (type.uniqueForAdditionalData) {
            dbConnector.removeAllWithTypeAndAdditionalData(type, additionalData)
        }

        val code = generateCode(sessionLogger, type)
        val actionCode = ActionCodeDB(
                type = type,
                code = code,
                created = TimeValue.now().milliseconds,
                additionalData = additionalData
        )
        dbConnector.put(actionCode)

        insertionsCount[type] = (insertionsCount[type] ?: 0) + 1

        return code
    }

    fun use(
            code: String,
            type: ActionCodeType,
            sessionLogger: HSessionLog
    ): String {

        val logger = sessionLogger.getMethodLog(
                ActionCodeManager::class.java,
                "use"
        )

        Validators.validateActionCodeOrThrow(
                actionCode = code,
                actionCodeType = type
        )

        val actionCode = dbConnector.findOrNull(type, code)
        if (actionCode == null) {
            logger.w("Code '$code' not found")
            throw ApiException.INCORRECT_ACTION_CODE
        }

        val created = actionCode.created?.let(::TimeValue) ?: TimeValue.ZERO
        if (created + type.lifetime < TimeValue.now()) {
            logger.w("Code '$code' outdated")
            throw ApiException.INCORRECT_ACTION_CODE
        }

        if (type.clearAfterCheck) {
            dbConnector.remove(type, code)
        }

        return actionCode.additionalData!!
    }

    private fun generateCode(
            sessionLogger: HSessionLog,
            type: ActionCodeType
    ): String {

        repeat(GENERATE_CODE_ATTEMPTS_COUNT) {
            val code = ActionCodeUtils.generate(type)
            if (!dbConnector.checkIsExists(type, code)) {
                return code
            }
        }

        sessionLogger
                .getMethodLog(ActionCodeManager::class.java, "generateCode")
                .e("Unable to generate unique code after $GENERATE_CODE_ATTEMPTS_COUNT attempts")

        throw ApiException.UNDEFINED
    }

    private fun cleanUpIfNeed(
            type: ActionCodeType,
            sessionLogger: HSessionLog
    ) {

        synchronized(ActionCodeManager) {
            val count = insertionsCount[type] ?: 0
            if (count < INSERTIONS_COUNT_UP_TO_CLEAN) {
                return
            }
            insertionsCount[type] = 0
        }

        val removedCount = dbConnector.removeAllWithTypeAndCreatedBeforeTime(
                type = type,
                beforeTime = System.currentTimeMillis() - type.lifetime.milliseconds
        ).takeIfPositive() ?: return

        sessionLogger
                .getMethodLog(ActionCodeManager::class.java, "cleanUpIfNeed")
                .d("Deleted $removedCount outdated codes of type $type")

    }


}
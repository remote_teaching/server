package ru.hnau.remote_teaching_server.managers.internal

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.handle
import ru.hnau.remote_teaching_common.data.section.SectionUtils
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.db.connectors.SectionDbConnector
import ru.hnau.remote_teaching_server.db.entities.SectionDB
import ru.hnau.remote_teaching_server.utils.Utils
import java.util.*


@Component
class SectionManager {

    @Autowired
    private lateinit var dbConnector: SectionDbConnector

    @Autowired
    private lateinit var testManager: TestManager

    companion object {

        fun validateAndPrepareSectionUUIDFromClient(
                sectionUUID: String
        ): String {
            if (sectionUUID == SectionUtils.ROOT_UUID) {
                return ""
            }
            try {
                UUID.fromString(sectionUUID)
            } catch (ex: IllegalArgumentException) {
                throw ApiException.raw("Некорректный идентификатор секции '$sectionUUID'")
            }
            return sectionUUID
        }

    }

    fun getSubsections(sectionUUID: String) =
            dbConnector.getSubskeletons(sectionUUID)

    fun getContentMD(sectionUUID: String) =
            sectionUUID.isEmpty().handle(
                    onTrue = { "" },
                    onFalse = { dbConnector.getContentMD(sectionUUID) }
            )


    fun addSubsection(
            sectionUUID: String,
            title: String
    ) = dbConnector.addSection(
            SectionDB(
                    parentUUID = sectionUUID,
                    uuid = Utils.genUUID(),
                    contentMD = "",
                    title = title
            )
    ).getSkeleton()

    fun removeSection(sectionUUID: String) {
        testManager.removeAllTestsOfSection(sectionUUID)
        getSubsections(sectionUUID).forEach { removeSection(it.uuid) }
        dbConnector.removeSection(sectionUUID)
    }

    fun updateTitle(
            sectionUUID: String,
            title: String
    ) = dbConnector.updateTitle(
            sectionUUID = sectionUUID,
            title = title
    )

    fun updateContentMD(
            sectionUUID: String,
            contentMD: String
    ) = dbConnector.updateContentMD(
            sectionUUID = sectionUUID,
            contentMD = contentMD
    )

}
package ru.hnau.remote_teaching_server.managers.internal

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_server.db.connectors.StudentsGroupDbConnector
import ru.hnau.remote_teaching_server.managers.internal.user.StudentManager
import ru.hnau.remote_teaching_server.managers.internal.user.UserManager


@Component
class StudentsGroupManager {

    @Autowired
    private lateinit var dbConnector: StudentsGroupDbConnector

    @Autowired
    private lateinit var userManager: UserManager

    @Autowired
    private lateinit var studentManager: StudentManager

    @Autowired
    private lateinit var testAttemptManager: TestAttemptManager

    fun getAll() =
            dbConnector.getAll().map { it.studentsGroup }

    fun throwIfArchived(name: String) =
            dbConnector.throwIfArchived(name)

    fun throwIfNotExists(name: String) =
            dbConnector.throwIfNotExists(name)

    fun create(name: String) {
        dbConnector.throwIfAlreadyExists(name)
        dbConnector.create(name)
    }

    fun remove(name: String) {
        dbConnector.throwIfNotArchived(name)
        testAttemptManager.removeAllStudentsGroupAttempts(name)
        studentManager.removeAllStudentsOfGroup(name)
        dbConnector.remove(name)
    }

    fun archive(name: String) {
        throwIfArchived(name)
        dbConnector.setIsArchived(name, true)
    }

    fun unarchive(name: String) {
        dbConnector.throwIfNotArchived(name)
        dbConnector.setIsArchived(name, false)
    }

}
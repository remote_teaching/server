package ru.hnau.remote_teaching_server.managers.internal

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.ifTrue
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.test.attempt.StudentsGroupTestResults
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.db.connectors.TestAttemptAnswerDbConnector
import ru.hnau.remote_teaching_server.db.entities.TestAttemptAnswerDB
import ru.hnau.remote_teaching_server.managers.internal.user.UserManager
import ru.hnau.remote_teaching_server.utils.TestAttemptAnswerScoreCalculator

@Component
class TestAttemptAnswerManager {

    @Autowired
    private lateinit var dbConnector: TestAttemptAnswerDbConnector

    @Autowired
    private lateinit var userManager: UserManager

    @Autowired
    private lateinit var testAttemptManager: TestAttemptManager

    @Autowired
    private lateinit var testManager: TestManager

    fun addAnswer(
            student: User,
            testAttemptUUID: String,
            answer: List<List<String>>
    ): Float {
        userManager.throwIfNotStudent(student)

        val testAttempt = testAttemptManager.getAttempt(testAttemptUUID)
        testAttempt.throwIfFinished()
        (testAttempt.studentsGroupName == student.studentsGroupName).ifFalse {
            throw ApiException.raw("Попытка $testAttemptUUID для группы ${testAttempt.studentsGroupName}, но студент в группе ${student.studentsGroupName}")
        }

        dbConnector.addAnswer(
                TestAttemptAnswerDB(
                        testAttemptUUID = testAttemptUUID,
                        studentLogin = student.login,
                        answer = answer,
                        timestamp = TimeValue.now().milliseconds
                )
        )

        val test = testManager.getTest(testAttempt.testUUID!!)
        val tasksCompilation = testAttemptManager.chooseTasksVariantsForStudentAttempt(
                testAttemptUUID = testAttemptUUID,
                studentLogin = student.login,
                tasks = test.tasks!!
        )

        val score = TestAttemptAnswerScoreCalculator.calc(
                tasksCompilation = tasksCompilation,
                answer = answer
        )

        return score
    }

    fun checkIfExists(
            testAttemptUUID: String,
            studentLogin: String
    ) = dbConnector.checkIsExists(
            testAttemptUUID = testAttemptUUID,
            studentLogin = studentLogin
    )

    fun getStudentsGroupTestResults(
            testUUID: String,
            studentsGroupName: String
    ) = testManager.getTest(testUUID).tasks!!.let { tasks ->
        testAttemptManager
                .getTestAttemptsForStudentGroup(
                        testUUID = testUUID,
                        studentsGroupName = studentsGroupName
                )
                .map { testAttempt ->
                    dbConnector.getAttemptAnswers(testAttempt.uuid!!)
                }
                .flatten()
                .groupBy { it.studentLogin!! }
                .mapValues { (_, studentAnswers) ->
                    studentAnswers.maxBy { it.timestamp!! }!!
                }
                .mapValues { (studentLogin, lastStudentAnswer) ->
                    TestAttemptAnswerScoreCalculator.calc(
                            answer = lastStudentAnswer.answer!!,
                            tasksCompilation = testAttemptManager.chooseTasksVariantsForStudentAttempt(
                                    studentLogin = studentLogin,
                                    testAttemptUUID = lastStudentAnswer.testAttemptUUID!!,
                                    tasks = tasks
                            )
                    )
                }
    }.let(::StudentsGroupTestResults)

    fun removeAllStudentAnswers(
            studentLogin: String
    ) = dbConnector.removeAllStudentAnswers(
            studentLogin = studentLogin
    )

    fun removeAllAttemptAnswers(
            testAttemptUUID: String
    ) = dbConnector.removeAllAttemptAnswers(
            testAttemptUUID = testAttemptUUID
    )


}
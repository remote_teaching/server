package ru.hnau.remote_teaching_server.managers.internal

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.ifTrue
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.notification.type.RTNotificationOnTestAttemptAdd
import ru.hnau.remote_teaching_common.data.test.TestTask
import ru.hnau.remote_teaching_common.data.test.attempt.TestAttempt
import ru.hnau.remote_teaching_common.data.test.attempt.tasks_compilation.TestAttemptTasksCompilation
import ru.hnau.remote_teaching_common.data.test.attempt.tasks_compilation.TestAttemptTasksCompilationTask
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.db.connectors.TestAttemptDbConnector
import ru.hnau.remote_teaching_server.db.entities.TestAttemptDB
import ru.hnau.remote_teaching_server.managers.internal.user.StudentManager
import ru.hnau.remote_teaching_server.managers.internal.user.UserManager
import ru.hnau.remote_teaching_server.managers.notifications.NotificationsManager
import ru.hnau.remote_teaching_server.utils.Utils
import ru.hnau.remote_teaching_server.utils.XorWowIntGenerator
import ru.hnau.remote_teaching_server.utils.log.HSessionLog
import kotlin.math.absoluteValue


@Component
class TestAttemptManager {

    @Autowired
    private lateinit var dbConnector: TestAttemptDbConnector

    @Autowired
    private lateinit var testManager: TestManager

    @Autowired
    private lateinit var studentsGroupManager: StudentsGroupManager

    @Autowired
    private lateinit var notificationsManager: NotificationsManager

    @Autowired
    private lateinit var studentManager: StudentManager

    @Autowired
    private lateinit var userManager: UserManager

    @Autowired
    private lateinit var testAttemptManager: TestAttemptManager

    @Autowired
    private lateinit var testAttemptAnswerManager: TestAttemptAnswerManager

    fun addAttempt(
            testUUID: String,
            studentsGroupName: String,
            timeLimit: Long,
            sessionLogger: HSessionLog
    ) {
        studentsGroupManager.throwIfArchived(studentsGroupName)
        val testSkeleton = testManager.getTest(testUUID).getSkeleton()
        val testAttemptDB = dbConnector.addAttempt(
                TestAttemptDB(
                        uuid = Utils.genUUID(),
                        studentsGroupName = studentsGroupName,
                        testUUID = testUUID,
                        timeLimit = timeLimit,
                        timestamp = System.currentTimeMillis()
                )
        )
        val testAttempt = testAttemptDB.getAttempt(
                testTitle = testSkeleton.title,
                timeLeft = testAttemptDB.timeLeft
        )
        val studentGroupUsers = studentManager.getAllStudentsOfGroup(studentsGroupName).map(User::login)
        notificationsManager.send(
                logins = studentGroupUsers,
                notification = RTNotificationOnTestAttemptAdd(testAttempt),
                sessionLogger = sessionLogger
        )
    }

    fun getAttempt(testAttemptUUID: String) =
            dbConnector.getAttempt(testAttemptUUID)

    fun getTestAttemptsForStudentGroup(
            testUUID: String,
            studentsGroupName: String
    ) = dbConnector.getTestAttemptsForStudentGroup(
            testUUID = testUUID,
            studentsGroupName = studentsGroupName
    )

    fun getAvailableTestAttemptsForStudent(
            student: User
    ): List<TestAttempt> {
        userManager.throwIfNotStudent(student)
        val studentsGroupName = student.studentsGroupName
        return dbConnector
                .getAttemptsForStudentsGroup(studentsGroupName)
                .mapNotNull { attempt ->

                    val timeLeft = attempt.timeLeft
                    if (timeLeft <= 0) {
                        return@mapNotNull null
                    }

                    testAttemptAnswerManager.checkIfExists(
                            studentLogin = student.login,
                            testAttemptUUID = attempt.uuid!!
                    ).ifTrue {
                        return@mapNotNull null
                    }

                    val testTitle = testManager.getTitle(attempt.testUUID!!)
                    attempt.getAttempt(
                            testTitle = testTitle,
                            timeLeft = timeLeft
                    )

                }
    }

    fun getTestAttemptTasksCompilation(
            testAttemptUUID: String,
            student: User
    ): TestAttemptTasksCompilation {
        userManager.throwIfNotStudent(student)
        val testAttempt = dbConnector.getAttempt(testAttemptUUID)
        if (testAttempt.studentsGroupName != student.studentsGroupName) {
            throw ApiException.raw("Попытка недоступна для группы студента ${student.login}")
        }
        if (testAttempt.timeLeft <= 0) {
            throw ApiException.raw("Время отведенное на попытку истекло")
        }
        val test = testManager.getTest(testAttempt.testUUID!!)
        return TestAttemptTasksCompilation(
                test = test.getSkeleton(),
                tasks = chooseTasksVariantsForStudentAttempt(
                        studentLogin = student.login,
                        testAttemptUUID = testAttemptUUID,
                        tasks = test.tasks!!
                ).map { it.copy(variant = it.variant.copy(responseParts = emptyList())) }
        )
    }

    fun chooseTasksVariantsForStudentAttempt(
            studentLogin: String,
            testAttemptUUID: String,
            tasks: List<TestTask>
    ): List<TestAttemptTasksCompilationTask> {
        val key = Utils.getStringsKey(studentLogin, testAttemptUUID)
        val seed = key.hashCode()
        val intGenerator = XorWowIntGenerator(seed)
        return tasks.map { it.chooseVariant(intGenerator) }
    }

    private fun TestTask.chooseVariant(
            xorWowIntGenerator: XorWowIntGenerator
    ) = TestAttemptTasksCompilationTask(
            maxScore = maxScore,
            type = type,
            variant = run {
                val variantsCount = variants.size
                val id = xorWowIntGenerator.next().absoluteValue
                val normalizedId = id % variantsCount
                variants[normalizedId]
            }
    )

    fun removeAllStudentsGroupAttempts(
            studentsGroupName: String
    ) {
        dbConnector.removeStudentsGroupAttempts(studentsGroupName)
        dbConnector.getStudentsGroupAttempts(studentsGroupName).forEach { attempt ->
            testAttemptAnswerManager.removeAllAttemptAnswers(attempt.uuid!!)
        }
        studentManager.getAllStudentsOfGroup(studentsGroupName).forEach { student ->
            testAttemptAnswerManager.removeAllStudentAnswers(student.login)
        }
    }

    fun removeAllTestAttempts(
            testUUID: String
    ) {
        dbConnector.getTestAttempts(testUUID).forEach { testAttempt ->
            testAttemptAnswerManager.removeAllAttemptAnswers(testAttempt.uuid!!)
        }
        dbConnector.removeTestAttempts(testUUID)
    }

}
package ru.hnau.remote_teaching_server.managers.internal

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.handle
import ru.hnau.jutils.ifTrue
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.UserRole
import ru.hnau.remote_teaching_common.data.section.SectionSkeleton
import ru.hnau.remote_teaching_common.data.test.TestSkeleton
import ru.hnau.remote_teaching_common.data.test.TestTask
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.db.connectors.TestDbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.entities.TestDB
import ru.hnau.remote_teaching_server.utils.Utils
import java.util.*
import kotlin.text.Typography.tm


@Component
class TestManager {

    @Autowired
    private lateinit var dbConnector: TestDbConnector

    @Autowired
    private lateinit var testAttemptManager: TestAttemptManager

    fun getTest(testUUID: String) =
            dbConnector.getTest(testUUID)

    fun getTestsOfSection(sectionUUID: String) =
            dbConnector.getTestsOfSection(sectionUUID)

    fun addTest(
            sectionUUID: String,
            title: String
    ) = dbConnector.addTest(
            TestDB(
                    uuid = Utils.genUUID(),
                    title = title,
                    sectionUUID = sectionUUID,
                    passScorePercentage = TestSkeleton.DEFAULT_PASS_SCORE_PERCENTAGE,
                    tasks = emptyList(),
                    timeLimit = TestSkeleton.DEFAULT_TIME_LIMIT
            )
    ).getSkeleton()

    fun getTasks(testUUID: String) =
            dbConnector.getTasks(testUUID)

    fun getTitle(uuid: String) =
            dbConnector.getTitle(uuid)

    fun updateTasks(
            testUUID: String,
            tasks: List<TestTask>
    ) = dbConnector.updateTasks(
            uuid = testUUID,
            tasks = tasks
    )

    fun updateTitle(
            testUUID: String,
            title: String
    ) = dbConnector.updateTitle(
            uuid = testUUID,
            title = title
    )

    fun updateTimeLimit(
            testUUID: String,
            timeLimit: Long
    ) = dbConnector.updateTimeLimit(
            uuid = testUUID,
            timeLimit = timeLimit
    )

    fun updatePassScorePercentage(
            testUUID: String,
            passScorePercentage: Float
    ) = dbConnector.updatePassScorePercentage(
            uuid = testUUID,
            passScorePercentage = passScorePercentage
    )

    fun removeTest(testUUID: String) {
        remveAllTestsAttemptsOfTest(testUUID)
        dbConnector.removeTest(testUUID)
    }

    fun removeAllTestsOfSection(sectionUUID: String) {
        dbConnector.getTestsOfSection(sectionUUID).forEach { test ->
            remveAllTestsAttemptsOfTest(test.uuid)
        }
        dbConnector.removeAllTestsOfSection(sectionUUID)
    }

    private fun remveAllTestsAttemptsOfTest(
            testUUID: String
    ) = testAttemptManager.removeAllTestAttempts(
            testUUID = testUUID
    )

    fun throwIfNotExists(testUUID: String) =
            dbConnector.throwIfNotExists(testUUID)

}
package ru.hnau.remote_teaching_server.managers.internal.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.takeIfNotEmpty
import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.UserRole
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.db.connectors.UserDbConnector
import ru.hnau.remote_teaching_server.db.entities.UserDB
import ru.hnau.remote_teaching_server.managers.internal.*
import ru.hnau.remote_teaching_server.utils.Utils
import ru.hnau.remote_teaching_server.utils.log.HSessionLog


@Component
class StudentManager {

    @Autowired
    private lateinit var dbConnector: UserDbConnector

    @Autowired
    private lateinit var userManager: UserManager

    @Autowired
    private lateinit var actionCodeManager: ActionCodeManager

    @Autowired
    private lateinit var studentsGroupManager: StudentsGroupManager

    @Autowired
    private lateinit var testAttemptManager: TestAttemptManager

    @Autowired
    private lateinit var testAttemptAnswerManager: TestAttemptAnswerManager

    fun generateCreateStudentActionCode(
            studentsGroupName: String,
            sessionLogger: HSessionLog
    ): String {

        studentsGroupManager.throwIfArchived(studentsGroupName)

        return actionCodeManager.create(
                type = ActionCodeType.CREATE_STUDENT_OF_GROUP,
                additionalData = studentsGroupName,
                sessionLogger = sessionLogger
        )
    }

    fun generateRestoreStudentPasswordActionCode(
            login: String,
            sessionLogger: HSessionLog
    ): String {

        dbConnector.throwIfNotStudent(login)

        return actionCodeManager.create(
                type = ActionCodeType.RESTORE_STUDENT_PASSWORD,
                additionalData = login,
                sessionLogger = sessionLogger
        )
    }

    fun restoreStudentPassword(
            code: String,
            newPassword: String,
            sessionLogger: HSessionLog
    ): String {

        val login = actionCodeManager.use(
                code = code,
                type = ActionCodeType.RESTORE_STUDENT_PASSWORD,
                sessionLogger = sessionLogger
        )

        userManager.updatePassword(login, newPassword)

        return login
    }

    fun createStudent(
            login: String,
            password: String,
            actionCode: String,
            sessionLogger: HSessionLog
    ) {
        dbConnector.throwIfAlreadyExists(login)

        val studentsGroupName = actionCodeManager.use(
                code = actionCode,
                type = ActionCodeType.CREATE_STUDENT_OF_GROUP,
                sessionLogger = sessionLogger
        )

        studentsGroupManager.throwIfArchived(studentsGroupName)

        userManager.createNew(
                login = login,
                password = password,
                role = UserRole.STUDENT,
                studentsGroupName = studentsGroupName
        )
    }

    fun getAllStudentsOfGroup(studentsGroupName: String): List<User> {
        studentsGroupManager.throwIfNotExists(studentsGroupName)
        return dbConnector.findAllStudentsOfGroup(studentsGroupName).toUsersList()
    }

    fun removeAllStudentsOfGroup(studentsGroupName: String) {
        dbConnector.removeAllStudentsOfGroup(studentsGroupName)
    }

    fun removeStudent(login: String) {
        testAttemptAnswerManager.removeAllStudentAnswers(login)
        dbConnector.throwIfNotStudent(login)
        dbConnector.remove(login)
    }

}
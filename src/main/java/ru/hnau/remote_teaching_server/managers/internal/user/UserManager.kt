package ru.hnau.remote_teaching_server.managers.internal.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.takeIfNotEmpty
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.UserRole
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.db.connectors.UserDbConnector
import ru.hnau.remote_teaching_server.db.entities.UserDB
import ru.hnau.remote_teaching_server.managers.internal.ClientAppInstanceManager
import ru.hnau.remote_teaching_server.utils.Utils
import ru.hnau.remote_teaching_server.utils.log.HSessionLog


@Component
class UserManager {

    @Autowired
    private lateinit var dbConnector: UserDbConnector

    @Autowired
    private lateinit var clientAppInstanceManager: ClientAppInstanceManager

    fun login(
            login: String,
            password: String,
            appInstanceUUID: String
    ): String {

        val user = dbConnector.getByLoginAndPasswordHashOrNull(
                login = login,
                passwordHash = UserDB.getPasswordHash(password)
        ) ?: throw ApiException.AUTHENTICATION

        clientAppInstanceManager.updateLogin(appInstanceUUID, login)

        var authToken = user.authToken?.takeIfNotEmpty()
        if (authToken == null) {
            authToken = Utils.genUUID()
            dbConnector.updateAuthToken(login, authToken)
        }

        return authToken
    }

    fun logout(
            login: String,
            fromAllClients: Boolean,
            appInstanceUUID: String
    ) {

        if (fromAllClients) {
            clientAppInstanceManager.cleanOutLogin(login)
            dbConnector.updateAuthToken(login, "")
            return
        }

        clientAppInstanceManager.clearLogin(appInstanceUUID)
    }

    fun updatePassword(login: String, newPassword: String) {
        dbConnector.updatePassword(
                login = login,
                newPasswordHash = UserDB.getPasswordHash(newPassword)
        )
    }

    fun updateFio(login: String, newName: String, newSurname: String, newPatronymic: String) {
        dbConnector.updateFio(login, newName, newSurname, newPatronymic)
    }

    fun getByAuthToken(authToken: String) =
            dbConnector.getByAuthTokenOrNull(authToken)
                    ?: throw ApiException.AUTHENTICATION

    fun createAdminIfNeed(sessionLogger: HSessionLog) {
        if (dbConnector.checkAdminIsExists()) {
            return
        }

        val logger = sessionLogger.getMethodLog(UserManager::class.java, "createAdminIfNeed")
        logger.d("There is no admin user, creating default admin user with login:'${User.ADMIN_LOGIN}' and password:'${User.DEFAULT_ADMIN_PASSWORD}'")
        createNew(
                login = User.ADMIN_LOGIN,
                password = User.DEFAULT_ADMIN_PASSWORD,
                role = UserRole.ADMIN
        )
    }

    fun createNew(
            login: String,
            password: String,
            role: UserRole,
            studentsGroupName: String = ""
    ) {
        val user = UserDB(
                login = login,
                role = role,
                studentsGroupName = studentsGroupName,
                name = "",
                passwordHash = UserDB.getPasswordHash(password),
                authToken = "",
                surname = "",
                patronymic = ""
        )
        dbConnector.put(user)
    }

    fun throwIfNotStudent(user: User) {
        (user.role != UserRole.STUDENT).ifTrue {
            throw ApiException.raw("Пользователь не является студентом")
        }
    }

}

fun Iterable<UserDB>.toUsersList() =
        map { it.user }
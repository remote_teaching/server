package ru.hnau.remote_teaching_server.managers.notifications

import com.google.firebase.messaging.FirebaseMessaging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.takeIfNotEmpty
import ru.hnau.jutils.tryCatch
import ru.hnau.remote_teaching_common.data.notification.RTNotification
import ru.hnau.remote_teaching_common.data.notification.RTToUserNotification
import ru.hnau.remote_teaching_server.managers.internal.ClientAppInstanceManager
import ru.hnau.remote_teaching_server.utils.log.HMethodLog
import ru.hnau.remote_teaching_server.utils.log.HSessionLog
import java.util.concurrent.Executors
import java.util.concurrent.Future

@Component
class NotificationsManager {

    @Autowired
    private lateinit var clientAppInstanceManager: ClientAppInstanceManager

    private val firebaseMessaging: FirebaseMessaging by lazy {
        FirebaseMessaging.getInstance()
    }

    private val sendNotificationCallbackExecutor =
            Executors.newSingleThreadExecutor()


    fun send(
            logins: Iterable<String>,
            notification: RTNotification,
            sessionLogger: HSessionLog
    ) {

        val serializedNotification =
                notification.serialize()

        val logger = sessionLogger.getMethodLog(NotificationsManager::class.java, "send")

        logins.forEach { toUserLogin ->

            val pushTokens = clientAppInstanceManager
                    .getPushTokensForLogin(toUserLogin)
                    .takeIfNotEmpty()
                    ?: return@forEach

            val toUserNotification = RTToUserNotification(
                    toUser = toUserLogin,
                    notification = serializedNotification
            )

            pushTokens.forEach { pushToken ->
                sendToUserAsync(
                        toUserNotification = toUserNotification,
                        pushToken = pushToken,
                        logger = logger
                )
            }

        }
    }

    private fun sendToUserAsync(
            toUserNotification: RTToUserNotification,
            pushToken: String,
            logger: HMethodLog
    ) {
        val message = toUserNotification.createMessage(pushToken)
        firebaseMessaging.sendAsync(message).let { future ->
            future.addListener(
                    Runnable {
                        handleSendResult(
                                toUserNotification,
                                future,
                                logger
                        )
                    },
                    sendNotificationCallbackExecutor
            )
        }
    }

    private fun handleSendResult(
            toUserNotification: RTToUserNotification,
            future: Future<String>,
            logger: HMethodLog
    ) {
        val login = toUserNotification.toUser
        tryCatch(
                throwsAction = {
                    val messageID = future.get()
                    logger.d("Sent to user '$login' message '$messageID'")
                },
                onThrow = { th ->
                    logger.w("Unable send message to user '$login': ${th.message}")
                }
        )
    }

}
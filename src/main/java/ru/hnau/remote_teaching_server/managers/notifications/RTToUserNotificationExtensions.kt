package ru.hnau.remote_teaching_server.managers.notifications

import com.google.firebase.messaging.Message
import ru.hnau.remote_teaching_common.data.notification.RTToUserNotification


fun RTToUserNotification.createMessage(
        pushToken: String
) = Message.builder()
        .putData(RTToUserNotification.SERIALIZATION_KEY_TO_USER, toUser)
        .putData(RTToUserNotification.SERIALIZATION_KEY_CLASS, notification.className)
        .putData(RTToUserNotification.SERIALIZATION_KEY_CONTENT, notification.json)
        .setToken(pushToken)
        .build()!!
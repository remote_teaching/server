package ru.hnau.remote_teaching_server.utils

import ru.hnau.jutils.handle
import ru.hnau.remote_teaching_common.data.test.TestTaskType
import ru.hnau.remote_teaching_common.data.test.TestTaskVariant
import ru.hnau.remote_teaching_common.data.test.attempt.tasks_compilation.TestAttemptTasksCompilationTask


object TestAttemptAnswerScoreCalculator {

    fun calc(
            tasksCompilation: List<TestAttemptTasksCompilationTask>,
            answer: List<List<String>>
    ): Float {
        val score = tasksCompilation.foldIndexed(0f) { i, acc, task ->
            val taskAnswer = answer.getOrNull(i) ?: return@foldIndexed acc
            acc + calcForTask(task, taskAnswer)
        }
        return (score * 10).toInt() / 10f
    }

    private fun calcForTask(
            task: TestAttemptTasksCompilationTask,
            taskAnswer: List<String>
    ): Float {
        val correctnessPercentage = calcCorrectnessPercentageForTask(
                taskType = task.type,
                variant = task.variant,
                taskAnswer = taskAnswer
        )
        return correctnessPercentage * task.maxScore
    }

    private fun calcCorrectnessPercentageForTask(
            taskType: TestTaskType,
            variant: TestTaskVariant,
            taskAnswer: List<String>
    ) = when (taskType) {
        TestTaskType.SINGLE -> calcCorrectnessPercentageForSingleTask(variant, taskAnswer)
        TestTaskType.MULTI -> calcCorrectnessPercentageForMultiTask(variant, taskAnswer)
        TestTaskType.TEXT -> calcCorrectnessPercentageForTextTask(variant, taskAnswer)
    }

    private fun calcCorrectnessPercentageForSingleTask(
            variant: TestTaskVariant,
            taskAnswer: List<String>
    ): Float {
        return (variant.responseParts[0] == taskAnswer.firstOrNull()).handle(
                forTrue = 1f,
                forFalse = 0f
        )
    }

    private fun calcCorrectnessPercentageForMultiTask(
            variant: TestTaskVariant,
            taskAnswer: List<String>
    ): Float {
        val correctParts = variant.responseParts.map(String::toInt).toSet()
        val taskAnswerParts = taskAnswer.map(String::toInt).toSet()
        val optionsCount = variant.optionsMD.size
        val errorsCount = (0 until optionsCount).count { i ->
            (i in taskAnswerParts) != (i in correctParts)
        }
        return 1f - errorsCount.toFloat() / optionsCount.toFloat()
    }

    private fun calcCorrectnessPercentageForTextTask(
            variant: TestTaskVariant,
            taskAnswer: List<String>
    ) = taskAnswer.firstOrNull()?.let {
        normalizeTextResponse(it).let { normalizedResponse ->
            variant.responseParts.any { oneOfCorrectResponses ->
                normalizedResponse == normalizeTextResponse(oneOfCorrectResponses)
            }.handle(
                    forTrue = 1f,
                    forFalse = 0f
            )
        }
    } ?: 0f

    private fun normalizeTextResponse(
            taskAnswer: String
    ): String {
        var whitespace = false
        var result = ""
        taskAnswer.toLowerCase().trim().forEach { char ->
            if (char.isWhitespace()) {
                if (!whitespace) {
                    result += " "
                    whitespace = true
                }
                return@forEach
            }
            result += char
            whitespace = false
        }
        return result
    }

}
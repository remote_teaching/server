package ru.hnau.remote_teaching_server.utils

import ru.hnau.jutils.tryCatch
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_common.utils.RANDOM
import java.io.PrintWriter
import java.io.StringWriter
import java.nio.ByteBuffer
import java.util.*
import java.util.UUID


object Utils {

    fun genUUID() = UUID.randomUUID().toString()

    fun genBase64UUID(): String {
        val uuid = UUID.randomUUID()
        val bb = ByteBuffer.wrap(ByteArray(16))
        bb.putLong(uuid.mostSignificantBits)
        bb.putLong(uuid.leastSignificantBits)
        return Base64.getEncoder().encodeToString(bb.array())
    }

    fun getStringsKey(vararg strings: String?) =
            strings.joinToString(
                    separator = "|",
                    transform = { string ->
                        string?.let { "${it.length}_$it" } ?: "0"
                    }
            )

}

val Exception.stringStackTrace: String
    get() {
        val stringWriter = StringWriter()
        printStackTrace(PrintWriter(stringWriter))
        return stringWriter.toString()
    }

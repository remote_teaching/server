package ru.hnau.remote_teaching_server.utils.log


class HMethodLog(
        sessionLog: HSessionLog,
        private val className: String,
        private val methodName: String
) : HLogWrapper(sessionLog) {

    constructor(
            sessionLog: HSessionLog,
            clazz: Class<*>,
            methodName: String
    ) : this(
            sessionLog,
            clazz.simpleName,
            methodName
    )

    override fun formatMessage(msg: String) = "[$className:$methodName] $msg"

}
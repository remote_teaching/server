package ru.hnau.remote_teaching_server.utils.log


class HSessionLog(
        private val sessionId: String
) : HLogWrapper(
        HBaseLog
) {

    override fun formatMessage(msg: String) = "{$sessionId} $msg"

    fun getMethodLog(className: String, methodName: String) = HMethodLog(this, className, methodName)

    fun getMethodLog(clazz: Class<*>, methodName: String) = HMethodLog(this, clazz, methodName)

}